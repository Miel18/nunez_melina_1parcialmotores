using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset; //desplasar
    Vector2 mM; //angulo que se decea mirar
    Vector2 sV;//movi del mouse al jugador
    public float sens = 5.0f; 
    public float suav = 2.0f; 
    GameObject jugador;

    void Start()
    {
        offset = transform.position - Jugador.transform.position;
        //pocicion de la camara, donde se ubicar� en otro posicion que este restar� desde el sitio del jugador. (recorrido, cambiado)
        jugador = this.transform.parent.gameObject;
        //El jugador es el padre de la c�mara
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")); //cambia el movimiento desde la ultima actualizacion
        md = Vector2.Scale(md, new Vector2(sens * suav, sens * suav));
        sV.x = Mathf.Lerp(sV.x, md.x, 1f / suav); //obtiene valores interpolado de x e y
        sV.y = Mathf.Lerp(sV.y, md.y, 1f / suav);
        mM += sV;
        mM.y = Mathf.Clamp(mM.y, -90f, 90f); //limita la rotacion (para no romerle la cabeza ja)
        transform.localRotation = Quaternion.AngleAxis(-mM.y, Vector3.right); //rota la cam de arriba a abajo
        jugador.transform.localRotation = Quaternion.AngleAxis(mM.x, jugador.transform.up);
    }

        

}
