using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    private Rigidbody RB;
    public TMPro.TMP_Text VGmas;
    public TMPro.TMP_Text Winer;
    private int cont;
    //declaraciones
    public LayerMask CP;
    public float fuerzaSalto; 
    public CapsuleCollider collr;
    //definimos una mascara (capa) para el piso. Definir una fuerza y setearle 10 en el editor
    public float Desplazado = 10.0f;
    public Camera C1P;
    public GameObject balaa;

    void Start()
    {
        RB = GetComponent<Rigidbody> ();
        //contador que empiese desde 0
        cont = 0;
        //VGmas estar� vacio
        Winer.text = "";
        setText();
        collr = GetComponent<CapsuleCollider> ();
        Cursor.lockState = CursorLockMode.Locked;//el cursor no se ver�

    }

    private void Update() 
    { 
        if (Input.GetKeyDown(KeyCode.Space) && SeEncuentraPiso()) //Verifica si esta el piso y mismo si esta apretando el epacio
        {
            RB.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse); //fuerza en salto
        }

        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; //para que aparezca el cursor denuevo
        }

        float MH = Input.GetAxis("Horizontal") * Desplazado;
        float MV = Input.GetAxis("Vertical") * Desplazado;
        Vector3 VM = new Vector3(MH, 0.0f, MV);
        MH *= Time.deltaTime;//por igual al tiempo de cada fotograma
        MV *= Time.deltaTime;
        transform.Translate(MH, 0, MV);
        if (Input.GetKeyDown("escape")) 
        { 
            Cursor.lockState = CursorLockMode.None; 
        }


        if (Input.GetMouseButtonDown(0)) //boton izquierdo del mouse esta en 0
        {
            Ray ray = C1P.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));//centro del puntero
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 20)//distancia del objeto tocado al origen del rayo)
            {//si el rallo toco algo muestra lo escrito ""
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }

            if (hit.collider.name.Substring(0, 3) == "Bot")//Pregunto por las 3 primeras letras delnombre del collider que toc� al rayo
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name); //Obtengo al objeto tocado
                BOT scriptObjetoTocado = (BOT)objetoTocado.GetComponent(typeof(BOT));//Obtengo al script del objeto tocado
                if (scriptObjetoTocado != null)//Invoco al script del objeto tocado
                {
                    scriptObjetoTocado.recibirDa�o();
                }
            }

            GameObject pro; //Instancio al prefab proyectil desde unorigen con la rotaci�n del jugador
            pro = Instantiate(balaa, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(C1P.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro, 5);//Destruyo al proyectil luego de 5 segundos 
        }
    }


    

    private bool SeEncuentraPiso() 
    { 
        return Physics.CheckCapsule(collr.bounds.center, new Vector3(collr.bounds.center.x, collr.bounds.min.y, collr.bounds.center.z), collr.radius * .9f, CP); 
        //Chequea si el collier esta tocando el piso
    }

    private void OnTriggerEnter(Collider other)
    {
        //Cuando trigger este acctivado dentro entonces si el objeto contiene VG puesto debe ser verdadero. Si otro objeto pasa por este, la activacio debe ser denegada(falso).
        if (other.gameObject.CompareTag("VG") == true)
        {
            other.gameObject.SetActive(false);
            //el contador incrementa (1+ = 1, +1 = 2...) y en el texto son actualizados
            cont = cont + 1;
            setText();
        }

    }

    private void setText()
    {
        VGmas.text = "VG: " + cont.ToString();
        //si contador es igual a 2 entonces debe verse "Winer" con el texto espesificado
        if (cont >= 2)
        {
            Winer.text = "GANASTE! ;)";
        }

    }

}
