using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOT : MonoBehaviour
{
    private int hp;
    private GameObject Jugador;
    public int rapidez;



    void Start() 
    { 
        hp = 100; //que empieze desde 100
        Jugador = GameObject.Find("Jugador");
    } 

    public void recibirDa�o() 
    { 
        hp = hp - 25; 
        
        if (hp <= 0) //si la vida es 0 este desaparece
        { 
            this.desaparecer(); 
        } 
    } 
    private void desaparecer() //destruyel al objeto
    { 
        Destroy(gameObject); 
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("BALA")) //todo que tenga la etiqueta BALA le baja la vida
        {
            recibirDa�o();
        }
    }

    private void Update() 
    { 
        transform.LookAt(Jugador.transform);//Avanza hacia adelante 
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime); //Rota seg�n la posici�n de otro objeto
    }
}
